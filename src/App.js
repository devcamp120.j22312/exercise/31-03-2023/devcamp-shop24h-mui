import Content from "./components/ContentComponent/Content";
import FooterComponent from "./components/FooterComponent/FooterComponent";
import Header from "./components/HeaderComponent/Header";


function App() {
  return (
    <div>
      <Header />
      <Content />
      <FooterComponent />
    </div>
  );
}

export default App;
