import "bootstrap/dist/css/bootstrap.min.css"
import { Row, Col, Container } from "reactstrap"

import FacebookIcon from '@mui/icons-material/Facebook';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import TwitterIcon from '@mui/icons-material/Twitter';

const FooterComponent = () => {
    return (
        <div className="bg-light" style={{paddingTop:"10px", marginTop:"10px"}}>
            <Container>
                <Row className="text-center mt-3">
                    <Col xs="8" className="">
                        <Row xs="3">
                            <Col className="">
                                <h6><b>PRODUCTS</b></h6>
                            </Col>
                            <Col className="">
                                <h6><b>SERVICES</b></h6>
                            </Col>
                            <Col className="">
                                <h6><b>SUPPORT</b></h6>
                            </Col>
                        </Row>
                        <Row xs="3">
                            <Col className="">
                                <p>Help Center</p>
                            </Col>
                            <Col className="">
                                <p>Help Center</p>
                            </Col>
                            <Col className="">
                                <p>Help Center</p>
                            </Col>
                        </Row>
                        <Row xs="3">
                            <Col className="">
                                <p>Contact Us</p>
                            </Col>
                            <Col className="">
                                <p>Contact Us</p>
                            </Col>
                            <Col className="">
                                <p>Contact Us</p>
                            </Col>
                        </Row>
                        <Row xs="3">
                            <Col className="">
                                <p>Product Help</p>
                            </Col>
                            <Col className="">
                                <p>Product Help</p>
                            </Col>
                            <Col className="">
                                <p>Product Help</p>
                            </Col>
                        </Row>
                        <Row xs="3">
                            <Col className="">
                                <p>Warranty</p>
                            </Col>
                            <Col className="">
                                <p>Warranty</p>
                            </Col>
                            <Col className="">
                                <p>Warranty</p>
                            </Col>
                        </Row>
                        <Row xs="3">
                            <Col className="">
                                <p>Order Status</p>
                            </Col>
                            <Col className="">
                                <p>Order Status</p>
                            </Col>
                            <Col className="">
                                <p>Order Status</p>
                            </Col>
                        </Row>

                    </Col>
                    <Col xs="4" className="mt-3">
                        <h3 style={{ fontWeight: "700" }}>Devcamp</h3>
                        <div>
                            <FacebookIcon style={{ margin: "10px" }} />
                            <YouTubeIcon style={{ margin: "10px" }} />
                            <TelegramIcon style={{ margin: "10px" }} />
                            <TwitterIcon style={{ margin: "10px" }} />
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default FooterComponent;