import { Navbar, NavbarBrand } from "reactstrap"
import avatar from "../../assets/images/logo.png"

import "bootstrap/dist/css/bootstrap.min.css"
import NotificationsIcon from '@mui/icons-material/Notifications';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

const Header = () => {
    return (
        <div>
            <Navbar
                className=""
                color="dark"
                dark
            >
                <NavbarBrand href="/">
                    <img
                        alt="logo"
                        src= {avatar}
                        style={{
                            height: 40,
                            width: 40,
                            marginRight: "10px"
                        }}
                    />
                    Devcamp
                </NavbarBrand>
                <div>
                    <NotificationsIcon style={{color:"white", margin:"10px"}} />
                    <AccountCircleIcon style={{color:"white", margin:"10px"}}/>
                    <ShoppingCartOutlinedIcon style={{color:"white", margin:"10px"}}/>
                </div>
            </Navbar>
        </div>
    )

}

export default Header;