import { Container, Row, Col, Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'

const LastestProducts = () => {
    return (
        <div>
            <div className="text-center mt-3">
                <h3><b>LASTEST PRODUCT</b></h3>
            </div>
            <Container className="mt-3">
                <Row className='text-center' sx="4">
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 1
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 2
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 3
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 4
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {/* Product hàng 2 */}
                <Row className='text-center mt-3' sx="4">
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 5
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 6
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 7
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <img
                                alt="Sample"
                                src="https://picsum.photos/300/200"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Product 8
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default LastestProducts;