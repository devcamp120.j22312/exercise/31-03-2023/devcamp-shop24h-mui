import CarouselComponent from "./CarouselComponent/CarouselComponent";
import LastestProducts from "./LastestProducts/LastestProducts";
import ViewAll from "./ViewAll/ViewAll";

const Content = () => {
    return(
        <div>
            <CarouselComponent />
            <LastestProducts />
            <ViewAll />
        </div>
    )
}

export default Content;