import { Button } from 'reactstrap'

const ViewAll = () => {
    return(
        <div className='text-center mt-3'>
            <Button color="dark">VIEW ALL</Button>
        </div> 
    )
}

export default ViewAll;