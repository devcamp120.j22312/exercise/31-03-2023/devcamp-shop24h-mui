// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo Schema
const Schema = mongoose.Schema;

// 
const customerSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model("Customer", customerSchema)