// Khai báo thư viện mongoose 
const mongoose = require('mongoose');

// Khai báo Schema
const Schema = mongoose.Schema;

// Khai báo Product Type 
const productTypeSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    }
}, {
    timestamps: true
})

// Biên dịch 
module.exports = mongoose.model("Product Type", productTypeSchema);