// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo Schema
const Schema = mongoose.Schema;

// 
const orderSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
    }],
    cost: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Order", orderSchema);