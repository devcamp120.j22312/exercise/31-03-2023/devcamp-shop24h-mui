// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import OrderDetail Model
const orderDetailModel = require('../models/orderDetailModel');

// Create
const createOrderDetail = (request, response) => {
    let body = request.body;

    // Validate
    if (!(Number.isInteger(body.quantity) && body.quantity > 0)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Quantity is not valid!"
        })
    }
    //
    let newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        quantity: body.quantity
    }
    orderDetailModel.create((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All
const getAllOrderDetail = (request, response) => {
    orderDetailModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error!',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Order Detail!',
                data: data
            })
        }
    })
}

// Get By Id
const getOrderDetailById = (request, response) => {
    // Chuẩn bị dữ liệu
    let orderDetailId = request.params.orderDetailId;

    // Validate
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'order Detail Id is invalid!'
        })
    }

    // Gọi model get customer by id
    orderDetailModel.findById(orderDetailId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Order Detail By Id!',
                data: data
            })
        }
    })
}

// Update By Id
const updateOrderDetailById = (request, response) => {
    // Chuẩn bị dữ liệu
    let orderDetailId = request.params.orderDetailId;
    let body = request.body;

    // Validate
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'CustomerId is invalid'
        })
    }
    if (!(Number.isInteger(body.quantity) && body.quantity > 0)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Quantity is not valid!"
        })
    }

    let updateOrderDetail = {
        quantity: body.quantity
    }

    // Gọi Model Update
    orderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error!',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

// Delete By Id
const deleteOrderDetailById = (request, response) => {
    // Chuẩn bị dữ liệu
    let orderDetailId = request.params.orderDetailId;

    // Validate 
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'orderDetailId is invalid'
        })
    }

    // Gọi Model
    orderDetailModel.findByIdAndDelete(orderDetailId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',

            })
        }
    })
}

// Export
module.exports = {
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById
}