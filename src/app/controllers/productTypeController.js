// Khai báo thư viện Mongoose
const mongoose = require('mongoose');

// Import Product Type Model
const productTypeModel = require('../models/productTypeModel');

// Create Product Type
const createProductType = (request, response) => {
    // Chuẩn bị dữ liệu
    let body = request.body;

    // Validate
    if(!body.name) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Name không hợp lệ'
        })
    }

    // Gọi Model tạo dữ liệu
    const newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }

    productTypeModel.create(newProductType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: `Internal server error: ${error.message}`
            })
        }
        else {
            return response.status(201).json({
                status: 'Successfully created!',
                data: data
            })
        }
    })
}

// Get All Product Type
const getAllProductType = (request, response) => {
    productTypeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get detail product type successfully!',
                data: data
            })
        }
    })
}

// Get Product Type By Id
const getProductTypeById = (request, response) => {
    // Thu thập dữ liệu
    let productTypeId = request.params.productTypeId;

    // Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Product Type Id is invalid'
        })
    }
    
    // Xử lý và trả về kết quả
    productTypeModel.findById(productTypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get detail of product type successfully',
                data: data
            })
        }
    })
}

// Update Product Type By Id
const updateProductTypeById = (request, response) => {
    // Thu thập dữ liệu
    let productTypeId = request.params.productTypeId;
    let body = request.body;

    // Validate 
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Product Type Id is invalid'
        })
    }

    let newProductType = {
        name: body.name,
        description: body.description
    }

    // Gọi Model update product type
    productTypeModel.findByIdAndUpdate(productTypeId, newProductType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Update Product Type Successfully',
                data: data
            })
        }
    })
}

// Delete Product Type By Id
const deleteProductTypeById = (request, response) => {
    // Chuẩn bị dữ liệu
    let productTypeId = request.params.productTypeId;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Product Type Id is invalid'
        })
    }

    // Xử lý và trả về kết quả
    productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!'
            })
        }
    })
}

// Export 
module.exports = {
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductTypeById
}