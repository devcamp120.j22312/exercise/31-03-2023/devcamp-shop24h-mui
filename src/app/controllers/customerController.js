// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Customer Model
const customerModel = require('../models/customerModel');

// Create
const createCustomer = (request, response) => {
    // Chuẩn bị dữ liệu
    let body = request.body;

    // Validate 
    if(body.fullName === undefined) {
        return response.status(400).json({
            status: 'Fullname is required!',
            message: error.message
        })
    }
    if(body.phone === undefined) {
        return response.status(400).json({
            status: 'Phone is required!',
            message: error.message
        })
    }
    if(body.email === undefined) {
        return response.status(400).json({
            status: 'Email is required!',
            message: error.message
        })
    }
    let newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    }

    // Gọi model tạo customer
    customerModel.create(newCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All
const getAllCustomer = (request, response) => {
    customerModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error!',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Customer!',
                data: data
            })
        }
    })
}

// Get Customer By Id
const getCustomerById = (request, response) => {
    // Chuẩn bị dữ liệu
    let customerId = request.params.customerId;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'CustomerID is invalid!'
        })
    }

    // Gọi model get customer by id
    customerModel.findById(customerId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Customer By Id!',
                data: data
            })
        }
    })
}

// Update Customer By Id
const updateCustomerById = (request, response) => {
    // Chuẩn bị dữ liệu
    let customerId = request.params.customerId;
    let body = request.body;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'CustomerId is invalid'
        })
    }
    if(body.fullName === undefined) {
        return response.status(400).json({
            status: 'Fullname is required!',
            message: error.message
        })
    }
    if(body.phone === undefined) {
        return response.status(400).json({
            status: 'Phone is required!',
            message: error.message
        })
    }
    if(body.email === undefined) {
        return response.status(400).json({
            status: 'Email is required!',
            message: error.message
        })
    }

    let updateCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    }

    // Gọi Model Update
    customerModel.findByIdAndUpdate(customerId, updateCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error!',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

// Delete Customer By Id
const deleteCustomerById = (request, response) => {
    // Chuẩn bị dữ liệu
    let customerId = request.params.customerId;

    // Validate 
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'CustomerId is invalid'
        })
    }

    // Gọi Model
    customerModel.findByIdAndDelete(customerId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',

            })
        }
    })
}

// Export 
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}