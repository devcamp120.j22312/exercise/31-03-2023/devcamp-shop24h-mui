// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Product Model
const productModel = require('../models/productModel');

// Create
const createProduct = (request, response) => {
    // Chuẩn bị dữ liệu
    let body = request.body;

    // Validate dữ liệu
    if(!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is invalid'
        })
    }
    if(!body.imageUrl) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'imageUrl is invalid'
        })
    }
    if(body.buyPrice === undefined || !(Number.isInteger(body.buyPrice) && body.buyPrice > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Buy Price is invalid'
        })
    }
    if(body.promotionPrice === undefined || !(Number.isInteger(body.promotionPrice) && body.promotionPrice > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Promotion Price is invalid'
        })
    }
    if(!(Number.isInteger(body.amount) && body.amount > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Amount is invalid'
        })
    }

    // Xử lý và trả về kết quả 
    let newProduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: mongoose.Types.ObjectId(),
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }

    // Gọi Model create Product
    productModel.create(newProduct, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Create Product Successfully!',
                data: data
            })
        }
    })
}

// Get All
const getAllProduct = (request, response) => {
    productModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get All Product Successfully!',
                data: data
            })
        }
    })
}

// Get By Id
const getProductById = (request, response) => {
    // Thu thập dữ liệu
    let productId = request.params.productId;
    

    // Validate
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'ProductId is invalid!'
        })
    }

    // Gọi model
    productModel.findById(productId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get Product Detail Successfully!',
                data: data
            })
        }
    })
}

// Update By Id
const updateProductById = (request, response) => {
    // Thu thập dữ liệu
    let productId = request.params.productId;
    let body = request.body;

    // Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'ProductId is invalid!'
        })
    }
    if(!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is invalid'
        })
    }
    if(!body.imageUrl) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'imageUrl is invalid'
        })
    }
    if(body.buyPrice === undefined || !(Number.isInteger(body.buyPrice) && body.buyPrice > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Buy Price is invalid'
        })
    }
    if(body.promotionPrice === undefined || !(Number.isInteger(body.promotionPrice) && body.promotionPrice > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Promotion Price is invalid'
        })
    }
    if(!(Number.isInteger(body.amount) && body.amount > 0)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Amount is invalid'
        })
    }

    let updateProduct = {
        name: body.name,
        description: body.description,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    };

    // Gọi Model Update Product
    productModel.findByIdAndUpdate(productId, updateProduct, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

// Delete By Id
const deleteProductById = (request, response) => {
    // Chuẩn bị dữ liệu
    let productId = request.params.productId;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: 'Bad request!',
            message: 'ProductId is invalid!'
        })
    }

    // Gọi Model
    productModel.findByIdAndDelete(productId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error!',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!'
            })
        }
    })
}

// Export 
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
}