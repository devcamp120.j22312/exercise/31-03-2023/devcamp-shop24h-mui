// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Order Model
const orderModel = require('../models/orderModel');

// Create
const createOrder = (request, response) => {
    // Chuẩn bị dữ liệu 
    let body = request.body;
    
    // Validate 
    // Gọi model tạo mới order
    let newOrder = {
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }

    orderModel.create(newOrder, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All Order
const getAllOrder = (request, response) => {
    orderModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Data',
                data: data
            })
        }
    })
}

// Get Order By Id
const getOrderById = (request, response) => {
    // Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    
    // Validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'OrderId is invalid'
        })
    }

    // Gọi Model
    orderModel.findById(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Order By Id',
                data: data
            })
        }
    })
}

// Update Order By Id
const updateOrderById = (request, response) => {
    // Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    let body = request.body;

    // Validate 
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'OrderId is invalid'
        })
    }

    let updateOrder = {
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }

    orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated',
                data: data
            })
        }
    })
}

// Delete Order By Id
const deleteOrderById = (request, response) => {
    let orderId = request.params.orderId;

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'OrderId is invalid'
        })
    }

    //
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
                data: data
            })
        }
    })
}

// Export 
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}