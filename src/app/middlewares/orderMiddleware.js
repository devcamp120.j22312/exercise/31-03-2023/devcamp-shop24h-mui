// Create Order
const createOrder = (request, response, next) => {
    console.log("Create Order Middleware");
    next();
}

// Get All
const getAllOrder = (request, response, next) => {
    console.log("Get All Order Middleware");
    next();
}

// Get By Id
const getOrderById = (request, response, next) => {
    console.log("Get Order By Id Middleware");
    next();
}

// Update By Id
const updateOrderById = (request, response, next) => {
    console.log("Update Order Middleware");
    next();
}

// Delete By Id
const deleteOrderById = (request, response, next) => {
    console.log("Delete Order Middleware");
    next();
}
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}