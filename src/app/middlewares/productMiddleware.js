// Create 
const createProduct = (request, response, next) => {
    console.log("Create Product Middleware");
    next();
}

// Get All
const getAllProduct = (request, response, next) => {
    console.log("Get All Product Middleware");
    next();
}

// Get By Id
const getProductById = (request, response, next) => {
    console.log("Get Product By Id Middleware");
    next();
}

// Update By Id
const updateProductById = (request, response, next) => {
    console.log("Update Product Middleware");
    next();
}

// Delete By Id
const deleteProductById = (request, response, next) => {
    console.log("Delete Product Middleware");
    next();
}

module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
}