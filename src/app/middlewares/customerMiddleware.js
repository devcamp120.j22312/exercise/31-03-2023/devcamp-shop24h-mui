// Create Customer
const createCustomer = (request, response, next) => {
    console.log("Create Customer Middleware");
    next();
}

// Get All
const getAllCustomer = (request, response, next) => {
    console.log("Get All Customer Middleware");
    next();
}

// Get By Id
const getCustomerById = (request, response, next) => {
    console.log("Get Customer By Id Middleware");
    next();
}

// Update By Id
const updateCustomerById = (request, response, next) => {
    console.log("Update Customer Middleware");
    next();
}

// Delete By Id
const deleteCustomerById = (request, response, next) => {
    console.log("Delete Customer Middleware");
    next();
}
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}