// Create OrderDetail
const createOrderDetail = (request, response, next) => {
    console.log("Create OrderDetail Middleware");
    next();
}

// Get All
const getAllOrderDetail = (request, response, next) => {
    console.log("Get All OrderDetail Middleware");
    next();
}

// Get By Id
const getOrderDetailById = (request, response, next) => {
    console.log("Get OrderDetail By Id Middleware");
    next();
}

// Update By Id
const updateOrderDetailById = (request, response, next) => {
    console.log("Update OrderDetail Middleware");
    next();
}

// Delete By Id
const deleteOrderDetailById = (request, response, next) => {
    console.log("Delete OrderDetail Middleware");
    next();
}
module.exports = {
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById
}