// Create 
const createProductType = (request, response, next) => {
    console.log("Create Product Type Middleware");
    next();
}

// Get All
const getAllProductType = (request, response, next) => {
    console.log("Get All Product Type Middleware");
    next();
}

// Get By Id
const getProductTypeById = (request, response, next) => {
    console.log("Get Product Type By Id Middleware");
    next();
}

// Update By Id
const updateProductTypeById = (request, response, next) => {
    console.log("Update Product Type Middleware");
    next();
}

// Delete By Id
const deleteProductTypeById = (request, response, next) => {
    console.log("Delete Product Type Middleware");
    next();
}

module.exports = {
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductTypeById
}