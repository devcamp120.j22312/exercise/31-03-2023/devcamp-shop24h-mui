// Khai báo thư viện Express
const express = require('express');

// Khai báo router app
const router = express.Router();

// Import Customer Middleware
const orderDetailMiddleware = require('../middlewares/orderDetailMiddleware');

// Import Customer Controller 
const orderDetailController = require('../controllers/orderDetailController');

// Create
router.post('/orderDetails', orderDetailMiddleware.createOrderDetail, orderDetailController.createOrderDetail);

// Get All
router.get('/orderDetails', orderDetailMiddleware.getAllOrderDetail, orderDetailController.getAllOrderDetail);

// Get By Id
router.get('/orderDetails/:orderDetailId', orderDetailMiddleware.getOrderDetailById, orderDetailController.getOrderDetailById);

// Update By Id
router.put('/orderDetails/:orderDetailId', orderDetailMiddleware.updateOrderDetailById, orderDetailController.updateOrderDetailById);

// Delete By Id
router.delete('/orderDetails/:orderDetailId', orderDetailMiddleware.deleteOrderDetailById, orderDetailController.deleteOrderDetailById);

module.exports = router;