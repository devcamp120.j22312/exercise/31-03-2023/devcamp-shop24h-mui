// Khai báo thư viện express
const express = require('express');

// Khai báo router app
const router = express.Router();

// Import productTypeMiddleware
const productTypeMiddleware = require('../middlewares/productTypeMiddleware');

// Import productTypeController
const productTypeController = require('../controllers/productTypeController');

// Create
router.post('/productTypes', productTypeMiddleware.createProductType, productTypeController.createProductType);

// Get all Product Type
router.get('/productTypes',productTypeMiddleware.getAllProductType, productTypeController.getAllProductType);

// Get By Id
router.get('/productTypes/:productTypeId',productTypeMiddleware.getProductTypeById, productTypeController.getProductTypeById);

// Update By Id
router.put('/productTypes/:productTypeId',productTypeMiddleware.updateProductTypeById, productTypeController.updateProductTypeById);

// Delete By Id
router.delete('/productTypes/:productTypeId',productTypeMiddleware.deleteProductTypeById, productTypeController.deleteProductTypeById);

module.exports = router;