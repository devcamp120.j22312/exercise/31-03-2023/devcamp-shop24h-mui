// Khai báo thư viện Express
const express = require('express');

// Khai báo router app
const router = express.Router();

// Import Product Middleware
const productMiddleware = require('../middlewares/productMiddleware');

// Import Product Controller
const productController = require('../controllers/productController');

// Create
router.post('/products', productMiddleware.createProduct, productController.createProduct);

// Get All
router.get('/products', productMiddleware.getAllProduct, productController.getAllProduct);

// Get By Id
router.get('/products/:productId', productMiddleware.getProductById, productController.getProductById);

// Update By Id
router.put('/products/:productId', productMiddleware.updateProductById, productController.updateProductById);

// Delete By Id
router.delete('/products/:productId', productMiddleware.deleteProductById, productController.deleteProductById);

module.exports = router;