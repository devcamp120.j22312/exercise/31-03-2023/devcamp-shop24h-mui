// Khai báo thư viện Express
const express = require('express');

// Khai báo router app
const router = express.Router();

// Import Order Middleware
const orderMiddleware = require('../middlewares/orderMiddleware');

// Import Order Controller
const orderController = require('../controllers/orderController');

// Create
router.post('/orders', orderMiddleware.createOrder, orderController.createOrder);

// Get All
router.get('/orders', orderMiddleware.getAllOrder, orderController.getAllOrder);

// Get Order By Id
router.get('/orders/:orderId', orderMiddleware.getOrderById, orderController.getOrderById);

// Update Order By Id
router.put('/orders/:orderId', orderMiddleware.updateOrderById, orderController.updateOrderById);

// Delete Order By Id
router.delete('/orders/:orderId', orderMiddleware.deleteOrderById, orderController.deleteOrderById);

module.exports = router;