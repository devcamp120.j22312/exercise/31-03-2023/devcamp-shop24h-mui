// Khai báo thư viện Express
const express = require('express');

// Khai báo router app
const router = express.Router();

// Import Customer Middleware
const customerMiddleware = require('../middlewares/customerMiddleware');

// Import Customer Controller 
const customerController = require('../controllers/customerController');

// Create
router.post('/customers', customerMiddleware.createCustomer, customerController.createCustomer);

// Get All
router.get('/customers', customerMiddleware.getAllCustomer, customerController.getAllCustomer);

// Get By Id
router.get('/customers/:customerId', customerMiddleware.getCustomerById, customerController.getCustomerById);

// Update By Id
router.put('/customers/:customerId', customerMiddleware.updateCustomerById, customerController.updateCustomerById);

// Delete By Id
router.delete('/customers/:customerId', customerMiddleware.deleteCustomerById, customerController.deleteCustomerById);

module.exports = router;